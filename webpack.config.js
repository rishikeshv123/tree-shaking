module.exports = {
  mode: "production",
  entry: "./index.js",
  output: {
    path: __dirname,
    filename: 'dist.js'
  },
  optimization: {
    usedExports: true,
  }
};
