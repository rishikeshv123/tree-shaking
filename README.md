# Tree Shaking

## Steps to reproduce tree shaking

1. clone this project
2. run "npm i -D" command to install dependencies
3. check file abc.js and you will see 3 functions
4. check file index.js and you will see we are using only 1 function
5. run "npm run build" command to generate bundled file
6. check file dist.js and you will see only 1 function which was being used in index.js and other two fucntions have been removed
